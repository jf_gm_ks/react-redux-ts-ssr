const path = require('path');
const { merge } = require('webpack-merge');
const baseConfig = require('./webpack.base');

const cssRules = {
    test: /\.(css|scss)$/,
    use: ["style-loader", "css-loader"],
}

const config = {
    entry: './src/client/index.tsx',
    output: {
        filename: 'app.js',
        path: path.resolve(__dirname,'dist','public')
    },
    module: {
        rules: [cssRules]
    }
}

module.exports = merge(baseConfig, config)