# Simple ToDo App

- [Dependencies](#dependencies)
- [Run project](#run-project)
- [Webpack Configuration](#webpack-configuration)

## Dependencies

### Production

- react
- react-dom
- react-router-dom
- express
- typescript
- cors
- dotenv

`npm i react react-dom react-router-dom express typescript cors dotenv`

### Development

- @babel/core
- @babel/node
- @babel/plugin-transform-runtime
- @babel/preset-env
- @babel/preset-react
- @babel/preset-typescript
- @types/cors
- @types/dotenv
- @types/express
- @types/react
- @types/react-dom
- @types/react-router-dom
- babel-loader
- css-loader
- file-loader
- nodemon
- npm-run-all
- style-loader
- webpack
- webpack-cli
- webpack-merge
- webpack-node-externals

`npm i  @babel/core @babel/node @babel/plugin-transform-runtime @babel/preset-env @babel/preset-react @babel/preset-typescript @types/cors @types/dotenv @types/express @types/react babel-loader css-loader file-loader nodemon npm-run-all style-loader webpack webpack-cli webpack-merge webpack-node-externals -D`

## Run project

- First you must create a `.env` file. You have `.example.env` as a sample.
    - you can use the actual values for development.
- Let's run `npm run build:dev` to generate the dist directory
- Now you can use `npm run dev` to run the project and watch file changes

## Webpack Configuration

Code base for client and server.

__webpack.base.js__

```js
const tsRules = {
    test: /\.ts$/,
    exclude: /node_modules/,
    use: ["ts-loader"]
}

const tsxRules = {
    test: /\.tsx$/,
    exclude: /node_modules/,
    use: ["babel-loader"]
}

const imgRules = {
    test: /\.(jpg|jpeg|png|gif|mp3|svg)$/,
    use: ["file-loader"],
}

module.exports = {
    resolve: {
        extensions: [".ts", ".tsx", ".js" ]
    },
    module: {
        rules: [tsRules, tsxRules, imgRules]
    }
}
```

__webpack.client.js__

```js
const path = require('path');
const { merge } = require('webpack-merge');
const baseConfig = require('./webpack.base');

const cssRules = {
    test: /\.(css|scss)$/,
    use: ["style-loader", "css-loader"],
}

const config = {
    entry: './src/client/index.tsx',
    output: {
        filename: 'app.js',
        path: path.resolve(__dirname,'dist','public')
    },
    module: {
        rules: [cssRules]
    }
}

module.exports = merge(baseConfig, config)
```

__webpack.server.js__
```js
const path = require('path');
const { merge } = require('webpack-merge');
const baseConfig = require('./webpack.base');
const webpackNodeExternals = require('webpack-node-externals')

const cssRules = {
    test: /\.(css|scss)$/,
    use: ["css-loader"],
}

const config = {
    target: 'node',
    entry: './src/server/index.ts',
    output: {
        filename: 'app.js',
        path: path.resolve(__dirname, 'dist'),
        publicPath: '/dist'
    },
    module: {
        rules: [cssRules]
    },
    externals: [webpackNodeExternals()]
}

module.exports = merge(baseConfig, config)
```