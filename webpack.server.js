const path = require('path');
const { merge } = require('webpack-merge');
const baseConfig = require('./webpack.base');
const webpackNodeExternals = require('webpack-node-externals')

const cssRules = {
    test: /\.(css|scss)$/,
    use: ["css-loader"],
}

const config = {
    target: 'node',
    entry: './src/server/index.ts',
    output: {
        filename: 'app.js',
        path: path.resolve(__dirname, 'dist'),
        publicPath: '/dist'
    },
    module: {
        rules: [cssRules]
    },
    externals: [webpackNodeExternals()]
}

module.exports = merge(baseConfig, config)