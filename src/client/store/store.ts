import { createStore } from 'redux';

/** Nombres de constantes de acciones */
export const CREATE_TASK = 'CREATE_TASK';
export const UPDATE_TASK = 'UPDATE_TASK';
export const REMOVE_TASK = 'REMOVE_TASK';

const initialState = {
    tasks: [{
        id:"MakeaToDo",
        name:"Make a ToDo",
        status:1
    }]
};
/** Reduce
 * Función pura que retorna el estado actual
 */
function reducer(state = initialState, action:any) {
    
    switch(action.type) {

        case CREATE_TASK:
            const newTask = [{
                id: action.payload.replace(' ','').trim(),
                name: action.payload.trim(),
                status: 1
            }]
            return {
                ...state,
                tasks: [
                    ...state.tasks,
                    ...newTask
                ]
            }

        case UPDATE_TASK:
            const updateTask = {
                ...action.payload
            }
            const filterTask = state.tasks.filter(t => t.id != action.payload.id)
            return {
                ...state,
                tasks: [
                    ...filterTask,
                    ...[updateTask]
                ]
            }

        case REMOVE_TASK:
            const filterTasks = state.tasks.filter(t => t.id != action.payload)
            return {
                ...state,
                tasks: filterTasks
            }

        default:
            return state
    }

}

/** Store
 * Almacenamiento de nuestro estado
 */
const store = createStore(reducer)

export default store