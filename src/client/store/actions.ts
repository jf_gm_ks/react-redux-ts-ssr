import { CREATE_TASK, UPDATE_TASK, REMOVE_TASK } from './store';

export const createTask = (payload:any) => {
    return {
        type: CREATE_TASK,
        payload
    }
}

export const updateTask = (payload:any) => {
    return {
        type: UPDATE_TASK,
        payload
    }
}

export const removeTask = (id:any) => {
    return {
        type: REMOVE_TASK,
        payload:id
    }
}

export default {
    createTask,
    updateTask,
    removeTask
}