import React from 'react';
import { Route } from 'react-router-dom';
import Home from '../pages/Home/Home';
import Todo from '../pages/Home/Todo';

const Routes = () => {
    return (
        <>
        <Route exact path="/" render={()=><Home/>} />
        <Route exact path="/todo" render={()=><Todo/>} />
        </>
    );
};

export default Routes;