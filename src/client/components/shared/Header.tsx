import React from 'react';
import { Link } from 'react-router-dom';
import '../../../public/styles/components/shared/header.css';

const Header = () => {
    return (
        <header className="header">

                <ul>

                    <li> <Link to="/"> Home </Link> </li>
                    <li> <Link to="/todo"> ToDo </Link> </li>

                </ul>

        </header>
    );
};

export default Header;