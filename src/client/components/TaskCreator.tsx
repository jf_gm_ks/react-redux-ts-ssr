import React, { useEffect, useRef, useState } from 'react';
import { connect } from 'react-redux';
import { createTask } from '../store/actions';
import PropTypes from 'prop-types';
import useTaskManager from './hooks/useTaskManager';
import '../../public/styles/components/task.css';
import '../../public/styles/components/taskCreator.css';

interface props {
    setShow: any
}


const TaskCreator = ({ setShow }:props) => {

    const [name, setName] = useState("")

    const { create } = useTaskManager()

    const nameInput = useRef<HTMLInputElement>(null)

    useEffect(() => {
        if(nameInput.current) {
            nameInput.current.focus()
        }
    }, [])

    const createHandler = (validateKey:any, key:any) => {

        if(validateKey && key != 'Enter') return

        if(name.trim().length <= 0) return
        create(name)
        setShow(false);

    }

    const changeHandler = (e:any) => {
        
        setName(e.target.value)

    }

    return (
        <div className="component-task">
            <input type="text" name="name" onChange={changeHandler} ref={nameInput} onKeyPress={(e) => createHandler(true, e.key)} />
            <div className="buttons-container">

                <button 
                className="secondary-button rounded"
                onClick={() => setShow(false)}
                >
                Cancelar
                </button>
                <button 
                className="secondary-button rounded"
                onClick={()=>createHandler(false,'')}
                >
                Crear
                </button>

            </div>
        </div>
    );
};

TaskCreator.propTypes = {
    createTask: PropTypes.func.isRequired,
    setShow: PropTypes.func.isRequired
}

const mapDispatchToProps = (dispatch:any) => ({
    createTask: (payload:any) => dispatch(createTask(payload)) 
})

export default connect(()=>{}, mapDispatchToProps)(TaskCreator);