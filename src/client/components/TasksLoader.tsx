import React from 'react';
import useTaskManager from './hooks/useTaskManager';
import Task from './Task';

interface props {
    status: any,
    backStatus: any,
    nextStatus: any
}

const TasksLoader = ({status, backStatus, nextStatus}:props) => {
    
    const { tasks, update } = useTaskManager();

    return (
        <>
        {tasks.map((task:any) => task.status === status ? 
            <Task 
                key={task.id}
                id={task.id}
                name={task.name} 
                backStatus={!backStatus ? 'no-display':''} nextStatus={!nextStatus ? 'no-display':''}
                backHandler={()=>update(task, status-1)} 
                nextHandler={()=>update(task, status+1)} 
            />
        : null)}
        </>
    );
};

export default TasksLoader;