import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { createTask, updateTask, removeTask } from '../../store/actions';

const useTaskManager = () => {

    const tasks = useSelector((state:any) => state.tasks);
    const dispatch = useDispatch();

    let create = (name:any) => {
        dispatch({
            ...createTask(name)
        })
    }

    let update = (task:any, status:any) => {
        dispatch({
            ...updateTask({...task, status})
        })
    }

    let remove = (id:any) => {
        dispatch({
            ...removeTask(id)
        })
    }

    return {
        tasks,
        create,
        update,
        remove
    }
};

export default useTaskManager;