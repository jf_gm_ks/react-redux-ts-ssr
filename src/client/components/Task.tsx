import React from 'react';
import PropTypes from 'prop-types';
import '../../public/styles/components/task.css'
import useTaskManager from './hooks/useTaskManager';

interface props {
    id:any
    name: any,
    backStatus: any,
    backHandler: any,
    nextStatus: any,
    nextHandler: any
}

const Task = ({id, name, backStatus, backHandler, nextStatus, nextHandler}:props) => {
    
    const { remove } = useTaskManager()
    
    return (
        <div className="component-task">
            <button className="remove-button" aria-label="remove task" onClick={() => remove(id)}>x</button>
            <p>{name}</p>
            <div className="buttons-container">

                <button 
                className={`secondary-button rounded ${backStatus}`}
                aria-label="move to previous panel"
                onClick={backHandler}
                >
                {"<"}
                </button>

                <button 
                className={`secondary-button rounded ${nextStatus}`}
                aria-label="move to next panel"
                onClick={nextHandler}
                >
                {">"}
                </button>

            </div>
        </div>
    );
};

Task.propTypes = {
    name: PropTypes.string.isRequired,
    backStatus: PropTypes.string,
    backHandler: PropTypes.func,
    nextStatus: PropTypes.string,
    nextHandler: PropTypes.func
}

export default Task;