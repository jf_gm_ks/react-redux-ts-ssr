import React from 'react';
import Header from '../shared/Header';
import PropTypes from 'prop-types';
import '../../../public/styles/components/shared/layout.css'

interface props {
    children: any,
    pageClass: any
}

const HomeLayout = ({children, pageClass}:props) => {
    return (
        <div className="layout">
            <Header/>
            <div className={`content ${pageClass}`}>
                {children}
            </div>
        </div>
    );
};

HomeLayout.propTypes = {
    pageClass: PropTypes.string.isRequired
}

export default HomeLayout;