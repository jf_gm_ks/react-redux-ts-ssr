import React, { useState } from 'react';
import HomeLayout from '../../components/layouts/HomeLayout';
import TaskCreator from '../../components/TaskCreator';
import TasksLoader from '../../components/TasksLoader';
import '../../../public/styles/pages/todo.css';

const Todo = () => {

    const [showCreate, setShowCreate] = useState(false);

    return (
        <HomeLayout pageClass="page-home-todo">

            <section>

                <h2>To Do</h2>

                {!showCreate && <div className="button-container">
                    <button className="primary-button rounded" onClick={() => setShowCreate(true)}/>
                </div>}

                {showCreate && <TaskCreator setShow={setShowCreate} />}
                <TasksLoader status={1} backStatus={false} nextStatus={true} />

            </section>

            <section>
                <h2>In progress</h2>
                <TasksLoader status={2} backStatus={true} nextStatus={true} />
            </section>

            <section>
                <h2>Done</h2>
                <TasksLoader status={3} backStatus={true} nextStatus={false} />
            </section>

        </HomeLayout>
    );
};

export default Todo;