import React from 'react';
import { Link } from 'react-router-dom';
import HomeLayout from '../../components/layouts/HomeLayout';
import '../../../public/styles/pages/home.css';

const Home = () => {

    return (
        <HomeLayout pageClass="page-home">
            <h1>To Do</h1>
            <div className="button-c">
                <Link className="primary-button rounded shadow" to="/todo">
                    Let's Start!
                </Link>
            </div>
        </HomeLayout>
    );
};

export default Home;