import React from 'react';
import { Switch } from 'react-router-dom';
import { Provider } from 'react-redux';
import store from './store/store'
import Routes from './routes';
import Header from './components/shared/Header';
import '../public/styles/app.css'

const App = () => {
    return (
        <Provider store={store}>
            <Header/>
            <Switch>
                <Routes/>
            </Switch>
        </Provider>
    );
};

export default App;