import 'dotenv/config';
import http from 'http';
import https from 'https';
import app from './app';

/** - - - Enviroment variables - - - */
const NODE_ENV = process.env.NODE_ENV;
const HOST = process.env.HOST;
const HTTP_PORT:any = process.env.HTTP_PORT;
const HTTPS_PORT:any = process.env.HTTPS_PORT;
const DEV_PORT = process.env.DEV_PORT;

const httpServer = http.createServer(app)
const httpsServer = https.createServer(app.get('ssl-options'),app)

const main = async() => {

    try {
        
        if(NODE_ENV === 'production') {

            await httpServer.listen(HTTP_PORT, HOST, () => console.log(`http server. host: ${HOST}, port: ${HTTP_PORT}`))
            await httpsServer.listen(HTTPS_PORT, HOST, () => console.log(`https server. host: ${HOST}, port: ${HTTPS_PORT}`))
    
        } else app.listen(DEV_PORT, () => console.log(`Server running on development mode on port ${DEV_PORT}`))

    } catch (e) {
        console.log(`Server error: ${e}`)
    }

}

main();