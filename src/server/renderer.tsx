import React from 'react';
import ReactDOM from 'react-dom/server';
import { StaticRouter } from 'react-router-dom';
import App from '../client/App';
import Layout from '../public/Layout';

export default (req:any) => {

    const context = {};

    return ReactDOM.renderToString(
        <Layout>
            <StaticRouter location={req.path} context={context}>
                <App/>
            </StaticRouter>
        </Layout>
    );

}