import express from 'express';
import cors from 'cors';
import renderer from './renderer';
import fs from 'fs';

const ENV = process.env.NODE_ENV;

const app = express();

// - - - - - - - Settings - - - - - - - - -
app.set('env', ENV)
if(ENV === 'production') {
    const KEY_PATH:any = process.env.KEY_PATH;
    const CERT_PATH:any = process.env.CERT_PATH
    const sslOptions = {
        key: fs.readFileSync(KEY_PATH),
        cert: fs.readFileSync(CERT_PATH)
    }
    app.set('ssl-options', sslOptions);
}

// - - - - - - - Middlewares - - - - - - - 
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(cors());
app.use(express.static('./dist/public'));

// - - - - - - - - Render - - - - - - - -
app.get('/*', (req, res) => {

    res.send(renderer(req));

})

export default app;