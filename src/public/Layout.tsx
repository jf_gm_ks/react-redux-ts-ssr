import React from 'react';

interface props {
    children: any;
}

const Layout = ({ children }:props) => {
    return (
        <html lang="en">
        <head>
            <meta charSet="UTF-8"/>
            <meta httpEquiv="X-UA-Compatible" content="IE=edge"/>
            <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
            <title>Webpack React Typescript SSR</title>
            <link rel="preconnect" href="https://fonts.googleapis.com"/>
            <link rel="preconnect" href="https://fonts.gstatic.com" crossOrigin={""} />
            <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;300;400;600;700&display=swap" rel="stylesheet"></link>
        </head>
        <body>

            <div id="root">{children}</div>
            <script src="app.js"></script>
            
        </body>
        </html>
    );
};

export default Layout;
