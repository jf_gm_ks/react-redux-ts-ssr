const tsRules = {
    test: /\.ts$/,
    exclude: /node_modules/,
    use: ["ts-loader"]
}

const tsxRules = {
    test: /\.tsx$/,
    exclude: /node_modules/,
    use: ["babel-loader"]
}

const imgRules = {
    test: /\.(jpg|jpeg|png|gif|mp3|svg)$/,
    use: ["file-loader"],
}

module.exports = {
    resolve: {
        extensions: [".ts", ".tsx", ".js" ]
    },
    module: {
        rules: [tsRules, tsxRules, imgRules]
    }
}